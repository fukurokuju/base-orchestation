SHELL := /usr/bin/env bash

changelog:
	git-cliff | uniq

changelog-compare:
	diff CHANGELOG <(git-cliff | uniq)

changelog-update:
	git-cliff | uniq > CHANGELOG

docs-build:
	mkdocs build -d public

docs-serve:
	mkdocs serve

lint:
	pre-commit run --all-files --show-diff-on-failure --color always

docker-lint:
	docker run --rm -i hadolint/hadolint < Dockerfile

policy-test:
	conftest test scaffold/main.tf --policy policy/scaffold.rego

add-gitlab-agent:
	docker run --pull=always --rm registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/cli:stable generate --agent-token $TOKEN --kas-address "K3S_MASTER_IP" | kubectl apply -f -
