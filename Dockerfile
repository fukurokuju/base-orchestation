FROM kroniak/ssh-client:3.15
SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

# system packages
RUN apk add --no-cache  \
    python3-dev=3.9.7-r4 \
    py3-pip=20.3.4-r1  \
    curl=7.80.0-r0 \
    make=4.3-r0 \
    ansible=4.8.0-r0 \
    gcc=10.3.1_git20211027-r0 \
    git=2.34.1-r0 \
    build-base=0.5-r2 \
    libffi-dev=3.4.2-r1 \
    shellcheck=0.7.2-r2

# pypi and ansible packages
RUN pip3 install --no-cache-dir \
    mkdocs-material==8.1.8 \
    pre-commit==2.17.0

# install k3sup
RUN curl -sLS https://get.k3sup.dev | sh

# install terraform
RUN curl -LO https://releases.hashicorp.com/terraform/1.1.4/terraform_1.1.4_linux_amd64.zip  && \
    unzip terraform_1.1.4_linux_amd64.zip && \
    mv terraform /usr/local/bin

# install conftest
RUN curl -L https://github.com/open-policy-agent/conftest/releases/download/v0.30.0/conftest_0.30.0_Linux_x86_64.tar.gz | tar xvz && \
    mv conftest /usr/local/bin

# add user
RUN adduser -D fuku
USER fuku
