variable "proxmox_host" {
  type        = string
  default     = "ramiel"
  description = "The hostname of the Proxmox server"
}

variable "template_name" {
  type        = string
  default     = "ubuntu-2004-cloudinit-template"
  description = "The name of the cloud-init template to use when creating a new qemu_vm resource"
}
