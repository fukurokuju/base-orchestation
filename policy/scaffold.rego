package main


has_field(obj, field) {
	obj[field]
}

deny[msg] {
    input.resource.proxmox_lxc[name].onboot != true
    msg = sprintf("`%v` lxc resource is not set to autoboot", [name])
}

deny[msg] {
    input.resource.proxmox_lxc[name].target_node != "ramiel"
    msg = sprintf("`%v` lxc resource's node is not not set to ramiel", [name])
}

deny[msg]{
    input.resource.proxmox_lxc[name].ostemplate != "storage:vztmpl/debian-11-standard_11.0-1_amd64.tar.gz"
    msg = sprintf("`%v` lxc resource's template is not set to storage:vztmpl/debian-11-standard_11.0-1_amd64.tar.gz", [name])
}

deny[msg] {
    input.resource.proxmox_lxc[name].start != true
    msg = sprintf("`%v` lxc resource is not set to start", [name])
}

deny[msg] {
    input.resource.proxmox_lxc[name].memory > "10240"
    msg = sprintf("`%v` lxc resource's memory is less than 10240", [name])
}

deny[msg] {
    input.resource.proxmox_lxc[name].memory > "14336"
    msg = sprintf("`%v` lxc resource's swap is less than 14336", [name])
}

deny[msg] {
    input.resource.proxmox_lxc[name].cores > 4
    msg = sprintf("`%v` lxc resource's cores is more than 4", [name])
}

deny[msg] {
    input.resource.proxmox_vm_qm[name].onboot != true
    msg = sprintf("`%v` qemu resource is not set to autoboot", [name])
}

deny[msg] {
    input.resource.proxmox_vm_qm[name].agent != 1
    msg = sprintf("`%v` qemu resource agent is not set", [name])
}

deny[msg] {
    input.resource.proxmox_vm_qm[name].os_type != "cloud-init"
    msg = sprintf("`%v` qemu resource os_type is not set as cloud-init", [name])
}

deny[msg] {
    input.resource.proxmox_vm_qm[name].memory > "10240"
    msg = sprintf("`%v` qemu resource's memory is less than 10240", [name])
}

deny[msg] {
    input.resource.proxmox_vm_qemu[name].core > 4
    msg = sprintf("`%v` qemu resource's cores is more than 4", [name])
}

deny[msg] {
    input.resource.proxmox_vm_qemu[name].disk.storage != "storage"
    msg = sprintf("`%v` qemu resource's disk storage is not set to 'storage'", [name])
}

deny[msg] {
    input.resource.proxmox_vm_qemu[name].network.bridge != "vmbr0"
    msg = sprintf("`%v` qemu resource's network bridge is not set to 'vmbr0'", [name])
}
